//
//  Definitions.h
//  AAP - App
//
//  Created by uni on 06/10/2019.
//

#ifndef Definitions_h
#define Definitions_h

const size_t irLength = 32895;

#define fSqrt32 sqrt(3.f) / 2.f
#define fSqrt58 sqrt(5.f / 8.f)
#define fSqrt152 sqrt(15.f) / 2.f
#define fSqrt38 sqrt(3.f / 8.f)

#endif /* Definitions_h */
