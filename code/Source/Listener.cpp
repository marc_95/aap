//
//  Listener.cpp
//  AAP - App
//
//  Created by uni on 05/10/2019.
//

#include "Listener.h"

aap::Listener::Listener()
{
    mOscManager = nullptr;
}

aap::Listener::Listener(int numberOfSources)
{
    for (int channel = 0; channel < numberOfSources; channel++)
    {
        mAudioSources.push_back(aap::AudioSource(channel));
    }
    mOscManager = nullptr;
}

aap::Listener::Listener(int numberOfSources, const OscManager& oscManager)
{
    for (int channel = 0; channel < numberOfSources; channel++)
    {
        mAudioSources.push_back(aap::AudioSource(channel));
    }
    mOscManager = &oscManager;
}

void aap::Listener::callibrateAngle()
{
    mAngle = 0;
    if (mOscManager != nullptr)
    {
        mAngle = mOscManager->getAzimuth();
    }
}

void aap::Listener::setAudioSources(std::vector<aap::AudioSource> audioSources)
{
    mAudioSources = audioSources;
}

void aap::Listener::addAudioSource(aap::AudioSource audioSource)
{
    mAudioSources.push_back(audioSource);
}

std::vector<aap::AudioSource>& aap::Listener::getAudioSources()
{
    return mAudioSources;
}

float aap::Listener::getAngle() const
{
    return mOscManager->getAzimuth() - mAngle;
}
