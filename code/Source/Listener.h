//
//  Listener.hpp
//  AAP - App
//
//  Created by uni on 05/10/2019.
//

#ifndef Listener_hpp
#define Listener_hpp

#include <stdio.h>
#include <vector>

#include "AudioSource.h"
#include "OscManager.h"

// This class contains all the members for the Listener
// Basically it will contain all the sources he is listening to and his angle with respect to them

// TODO source class and include it here

namespace aap
{
    class Listener;
}

class aap::Listener
{
public:
    Listener();
    explicit Listener(int numberOfSources, const OscManager& oscManager);
    explicit Listener(int numberOfSources);
    void callibrateAngle();
    void setAudioSources(std::vector<aap::AudioSource> audioSources);
    void addAudioSource(aap::AudioSource audioSource);

    float getAngle() const;
    std::vector<aap::AudioSource>& getAudioSources();

private:
    // Angle of head rotation
    float mAngle                                = 0;
    std::vector<aap::AudioSource> mAudioSources = {};
    OscManager const* mOscManager;
};

#endif /* Listener_hpp */
