//
//  UIComponents.cpp
//  AAP - App
//
//  Created by uni on 12/10/2019.
//

#include "UIComponents.h"

UIComponents::UIComponents(int numOfInputs)
{
    mVolumeSlider.setRange(0.0, 1.0);
    mVolumeSlider.setSliderStyle(Slider::LinearVertical);
    mVolumeSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    mVolumeLabel.setText("Volume", dontSendNotification);
    mVolumeLabel.attachToComponent(&mVolumeSlider, false);

    for (int input = 0; input < numOfInputs; input++)
    {
        std::unique_ptr<Slider> azimuthSlider = std::make_unique<Slider>();
        std::unique_ptr<Label> azimuthLabel   = std::make_unique<Label>();
        azimuthSlider->setRange(-180.0, 180.0);
        azimuthSlider->setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
        azimuthSlider->setSliderStyle(Slider::Rotary);
        azimuthSlider->setComponentID("AZI");
        mSourcesAzimuthSlider.push_back(std::move(azimuthSlider));
        std::string azimuthLabelText = std::string("AZI ") + std::to_string(input + 1);
        azimuthLabel->setText(azimuthLabelText, dontSendNotification);
        azimuthLabel->attachToComponent(&*mSourcesAzimuthSlider[input], false);
        mSourcesAzimuthLabel.push_back(std::move(azimuthLabel));

        std::unique_ptr<Slider> elevationSlider = std::make_unique<Slider>();
        std::unique_ptr<Label> elevationLabel   = std::make_unique<Label>();
        elevationSlider->setRange(-180.0, 180.0);
        elevationSlider->setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
        elevationSlider->setComponentID("ELE");
        mSourcesElevationSlider.push_back(std::move(elevationSlider));
        std::string elevationLabelText = std::string("ELE ") + std::to_string(input + 1);
        elevationLabel->setText(elevationLabelText, dontSendNotification);
        elevationLabel->attachToComponent(&*mSourcesElevationSlider[input], false);
        mSourcesAzimuthLabel.push_back(std::move(elevationLabel));

        std::string activeButtonString = std::string("Activate ch ") + std::to_string(input + 1);
        std::unique_ptr<ToggleButton> sourceActive =
            std::make_unique<ToggleButton>(activeButtonString);
        sourceActive->setState(ToggleButton::ButtonState::buttonDown);
        mSourcesActive.push_back(std::move(sourceActive));
        
        mCallibrateHeadTracking = std::make_unique<TextButton>("Callibrate HT");
    }
}

float UIComponents::getVolumeValue() const
{
    return mVolumeSlider.getValue();
}

void UIComponents::resize(int width, int height)
{
    int numOfSources   = static_cast<int>(mSourcesActive.size());
    int widthInterval  = width / (numOfSources + 2);
    int heightInterval = height / (numOfSources + 3);

    int initialWidthPosition = 0;

    for (auto& activeButton : mSourcesActive)
    {
        initialWidthPosition += widthInterval;
        activeButton->setBounds(initialWidthPosition, 0, widthInterval / 2, heightInterval / 2);
    }
    initialWidthPosition = 0;
    for (auto& azimuthSlider : mSourcesAzimuthSlider)
    {
        initialWidthPosition += widthInterval;
        azimuthSlider->setBounds(
            initialWidthPosition, heightInterval, widthInterval, heightInterval);
    }

    mVolumeSlider.setBounds(width - width / 10, heightInterval * 2, widthInterval / 4, height / 2);

    int initialHeightPosition = heightInterval * 3;
    for (auto& elevationSlider : mSourcesElevationSlider)
    {
        elevationSlider->setBounds(widthInterval, initialHeightPosition, width * 2 / 3, 40);
        initialHeightPosition += heightInterval;
    }
    
    mCallibrateHeadTracking->setBounds(10, 10, widthInterval/2, heightInterval/2);
}
