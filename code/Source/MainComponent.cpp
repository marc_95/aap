/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "../External_libs/FFTConvolver/FFTConvolver.h"
#include "../External_libs/FFTConvolver/TwoStageFFTConvolver.h"
#include "AudioSource.h"
#include "HrtfManager.h"

//==============================================================================
MainComponent::MainComponent()
: mUIComponents(mInputChannels)
, mOscManager()
{
    mListener   = aap::Listener(mInputChannels, mOscManager);
    int channel = 0;
    for (auto& activeButton : mUIComponents.mSourcesActive)
    {
        addAndMakeVisible(*activeButton);
        activeButton->addListener(&mListener.getAudioSources()[channel++]);
    }
    channel = 0;
    for (auto& azimuthSlider : mUIComponents.mSourcesAzimuthSlider)
    {
        addAndMakeVisible(*azimuthSlider);
        azimuthSlider->addListener(&mListener.getAudioSources()[channel++]);
    }
    channel = 0;
    for (auto& elevationSlider : mUIComponents.mSourcesElevationSlider)
    {
        addAndMakeVisible(*elevationSlider);
        elevationSlider->addListener(&mListener.getAudioSources()[channel++]);
    }
    addAndMakeVisible(mUIComponents.mVolumeSlider);

    mUIComponents.mCallibrateHeadTracking->onClick = [&]() { mListener.callibrateAngle(); };
    addAndMakeVisible(*mUIComponents.mCallibrateHeadTracking);

    // Make sure you set the size of the component after
    // you add any child components.
    setSize(800, 600);

    mHrtfManager = HrtfManager(mAmbiOrder);
    // TODO this listener will be shared with OSC thread

    // Some platforms require permissions to open input channels so request that here
    if (RuntimePermissions::isRequired(RuntimePermissions::recordAudio) &&
        !RuntimePermissions::isGranted(RuntimePermissions::recordAudio))
    {
        RuntimePermissions::request(RuntimePermissions::recordAudio, [&](bool granted) {
            if (granted)
                setAudioChannels(mInputChannels, mOutputChannels);
        });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels(mInputChannels, mOutputChannels);
    }
}

MainComponent::~MainComponent()
{
    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
    // This function will be called when the audio device is started, or when
    // its settings (i.e. sample rate, block size, etc) are changed.

    // You can use this function to initialise any resources you might need,
    // but be careful - it will be called on the audio thread, not the GUI thread.

    // For more details, see the help for AudioProcessor::prepareToPlay()

    mHrtfManager.loadConfig(mAmbiOrder);
    const size_t numOfAmbiChannels = mHrtfManager.getDecodingMatrix().size();
    const size_t numOfSpeakers     = mHrtfManager.getDecodingMatrix()[0].size();
    // reserve space to codec matrix
    mEncodedMatrix =
        std::vector<std::vector<float>>(numOfAmbiChannels, std::vector<float>(mBlockSize));
    mDecodedMatrix =
        std::vector<std::array<float, mBlockSize>>(numOfSpeakers, std::array<float, mBlockSize>());

    //    setAudioChannels(mInputChannels, mOutputChannels);
    //
    for (size_t speaker = 0; speaker < numOfSpeakers; speaker++)
    {
        mHrtfManager.getLeftConvolvers()[speaker].get()->init(
            mBlockSize, irLength, mHrtfManager.getLeftHrtfValues()[speaker].data(), irLength);
        mHrtfManager.getRightConvolvers()[speaker].get()->init(
            mBlockSize, irLength, mHrtfManager.getRightHrtfValues()[speaker].data(), irLength);
    }
}

void MainComponent::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill)
{
    // Your audio-processing code goes here!

    // For more details, see the help for AudioProcessor::getNextAudioBlock()
    AudioIODevice* device = deviceManager.getCurrentAudioDevice();
    // TODO this value should depend on the inputChannels from the UI (see member variable
    // mInputChannels)
    auto activeInputChannels   = device->getActiveInputChannels();
    const int maxInputChannels = activeInputChannels.getHighestBit() + 1;

    std::for_each(mEncodedMatrix.begin(), mEncodedMatrix.end(), [](auto& ambiChan) {
        ambiChan.clear();
    });
    for (int inputChannel = 0; inputChannel < maxInputChannels; inputChannel++)
    {
        const float* inBuffer = bufferToFill.buffer->getReadPointer(inputChannel);

        // TODO LPF FOR AZIMUTH, ELEVATION?
        auto& audioSource = mListener.getAudioSources()[inputChannel];

        // encoding
        std::vector<float> encodingMatrix = mHrtfManager.encodingMatrix(
            audioSource.getAzimuth() - mListener.getAngle(), audioSource.getElevation());
        if (inputChannel == 0)
        {
            mEncodedMatrix.reserve(encodingMatrix.size());
        }
        for (size_t ambiChannel = 0; ambiChannel < encodingMatrix.size(); ambiChannel++)
        {
            if (inputChannel == 0)
            {
                mEncodedMatrix[ambiChannel].reserve(mBlockSize);
                mEncodedMatrix[ambiChannel] = std::vector<float>(mBlockSize);
            }
            // check if source is active (not muted), if not don't encode!
            if (audioSource.isActive())
            {
                for (size_t sampleIdx = 0; sampleIdx < mBlockSize; sampleIdx++)
                {
                    mEncodedMatrix[ambiChannel][sampleIdx] +=
                        inBuffer[sampleIdx] * encodingMatrix[ambiChannel];
                }
            }
        }
    }

    // decoding
    mDecodedMatrix.reserve(mHrtfManager.getDecodingMatrix().size());
    for (size_t speaker = 0; speaker < mHrtfManager.getDecodingMatrix().size(); speaker++)
    {
        // mDecodedMatrix[speaker].reserve(mBlockSize);
        // init to zeros
        std::fill(std::begin(mDecodedMatrix[speaker]), std::end(mDecodedMatrix[speaker]), 0);
    }
    for (size_t speaker = 0; speaker < mHrtfManager.getDecodingMatrix().size(); speaker++)
    {
        for (size_t sampleIdx = 0; sampleIdx < mBlockSize; sampleIdx++)
        {
            for (size_t ambiChannel = 0; ambiChannel < mEncodedMatrix.size(); ambiChannel++)
            {
                mDecodedMatrix[speaker][sampleIdx] +=
                    mHrtfManager.getDecodingMatrix()[speaker][ambiChannel] *
                    mEncodedMatrix[ambiChannel][sampleIdx];
            }
        }
    }

    // Convolutions
    for (size_t outChannel = 0; outChannel < mOutputChannels; outChannel++)
    {
        float* outBuffer =
            bufferToFill.buffer->getWritePointer(outChannel, bufferToFill.startSample);
        std::array<float, mBlockSize> output = {0};
        if (outChannel == 0)
        {
            for (size_t speaker = 0; speaker < mHrtfManager.getLeftConvolvers().size(); speaker++)
            {
                std::vector<float> convOutput(mBlockSize);
                mHrtfManager.getLeftConvolvers()[speaker]->process(
                    mDecodedMatrix[speaker].data(), convOutput.data(), mBlockSize);
                for (size_t sampleIdx = 0; sampleIdx < mBlockSize; sampleIdx++)
                {
                    output[sampleIdx] +=
                        convOutput[sampleIdx] * mUIComponents.mVolumeSlider.getValue();
                }
            }
        }
        else
        {
            for (size_t speaker = 0; speaker < mHrtfManager.getRightConvolvers().size(); speaker++)
            {
                std::vector<float> convOutput(mBlockSize);
                mHrtfManager.getRightConvolvers()[speaker]->process(
                    mDecodedMatrix[speaker].data(), convOutput.data(), mBlockSize);
                for (size_t sampleIdx = 0; sampleIdx < mBlockSize; sampleIdx++)
                {
                    output[sampleIdx] +=
                        convOutput[sampleIdx] * mUIComponents.mVolumeSlider.getValue();
                }
            }
        }
        memcpy(outBuffer, output.data(), mBlockSize * sizeof(float));
    }
}

void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()
}

//==============================================================================
void MainComponent::paint(Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));

    // You can add your drawing code here!
}

void MainComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    mUIComponents.resize(getWidth(), getHeight());
}
