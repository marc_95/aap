//
//  AudioSource.cpp
//  AAP - App
//
//  Created by uni on 05/10/2019.
//

#include "AudioSource.h"

aap::AudioSource::AudioSource(int inputChannel)
: mInputChannel(inputChannel)
{
}

aap::AudioSource::~AudioSource()
{
}

void aap::AudioSource::setAzimuth(float azimtuh)
{
    mAzimuth = azimtuh;
}

void aap::AudioSource::setElevation(float elevation)
{
    mElevation = elevation;
}

void aap::AudioSource::setActive(bool isActive)
{
    mActive = isActive;
}

void aap::AudioSource::setInputChannel(int inputChannel)
{
    mInputChannel = inputChannel;
}

float aap::AudioSource::getAzimuth() const
{
    return mAzimuth;
}

float aap::AudioSource::getElevation() const
{
    return mElevation;
}

int aap::AudioSource::getInputChannel() const
{
    return mInputChannel;
}

std::pair<float, float> aap::AudioSource::getAzimuthElevation() const
{
    // TODO implement
    return std::pair<float, float>(mAzimuth, mElevation);
}

bool aap::AudioSource::isActive() const
{
    return mActive;
}

void aap::AudioSource::sliderValueChanged(Slider* slider)
{
    if (slider->getComponentID().toStdString() == std::string("AZI"))
    {
        setAzimuth(slider->getValue());
    }
    if (slider->getComponentID().toStdString() == std::string("ELE"))
    {
        setElevation(slider->getValue());
    }
}

void aap::AudioSource::buttonClicked(Button* button)
{
    setActive(button->getToggleState());
}

aap::AudioSource::AudioSource(const AudioSource& originalAudioSource)
{
    AudioSource(originalAudioSource.mInputChannel);
}
