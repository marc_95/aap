//
//  OscManager.cpp
//  AAP - App
//
//  Created by uni on 16/11/2019.
//

#include "OscManager.h"

#include <iostream>
#include <thread>

OscManager::OscManager(int port)
: mAzimuth(0)
, mStopThread(false)
, mPort(port)
{
    std::thread t(&OscManager::run, this);
    t.detach();
}

OscManager::~OscManager()
{
    mStopThread = true;
    mSocket->Break();
}

void OscManager::run()
{
    mSocket = std::make_unique<UdpListeningReceiveSocket>(IpEndpointName(IpEndpointName::ANY_ADDRESS, mPort), this);
    while (!mStopThread)
    {
        mSocket->Run();
    }
    mStopThread = false;
}

void OscManager::changePort(int port)
{
    mStopThread = true;
    mSocket->Break();
    mPort       = port;
    while (mStopThread)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    mSocket = std::make_unique<UdpListeningReceiveSocket>(IpEndpointName(IpEndpointName::ANY_ADDRESS, mPort), this);
    std::thread t(&OscManager::run, this);
    t.detach();
}

void OscManager::ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint)
{
    (void)remoteEndpoint; // suppress unused parameter warning

    try
    {
        float azi = 0;
        if (std::strcmp(m.AddressPattern(), "/orientation/azimuth") == 0)
        {
            osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
            args >> azi >> osc::EndMessage;
            mAzimuth = azi;
        }
    }
    catch (osc::Exception& e)
    {
        // any parsing errors such as unexpected argument types, or
        // missing arguments get thrown as exceptions.
        std::cout << "error while parsing message: " << m.AddressPattern() << ": " << e.what()
                  << "\n";
    }
}

float OscManager::getAzimuth() const
{
    return mAzimuth;
}
