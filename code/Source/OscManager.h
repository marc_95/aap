
#ifndef OscManager_h
#define OscManager_h

#include "../External_libs/osc/ip/UdpSocket.h"
#include "../External_libs/osc/osc/OscPacketListener.h"
#include "../External_libs/osc/osc/OscReceivedElements.h"

#include <atomic>
#include <memory>

class OscManager : public osc::OscPacketListener
{
public:
    OscManager(int udpPort = 57120);
    ~OscManager();
    void changePort(int port);
    float getAzimuth() const;

private:
    void
    ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint) override;
    void run();
    
    std::atomic<float> mAzimuth;
    bool mStopThread;
    int mPort;
    std::unique_ptr<UdpListeningReceiveSocket> mSocket;
};
#endif /* OscManager_h */
