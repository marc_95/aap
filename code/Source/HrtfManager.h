//
//  HrtfManager.h
//  AAP - App
//
//  Created by uni on 28/09/2019.
//

#ifndef HrtfManager_h
#define HrtfManager_h

#include <array>
#include <string>
#include <vector>

#include "Definitions.h"

#include <../External_libs/FFTConvolver/TwoStageFFTConvolver.h>

// This class should be able to load the HRTF and it's corresponding decoding matrix.
// Before loading them you can ask which HRTF are available through a static function,
// for memory managment purposes it cannot have more than one HRTF dataset loaded at the same time.

// TODO, now it takes the config by an index and the values for hrtfs and decoding matrix are
// harcoded, in the future should be able to navigate through folders and read a config file (pass
// path around instead of index)
class HrtfManager
{
public:
    enum class DecodingMatrixOrder
    {
        FIRST_ORDER,
        FIRST_VALUE = FIRST_ORDER,
        SECOND_ORDER,
        THIRD_ORDER,
        FOURTH_ORDER,
        FIFTH_ORDER,
        LAST_VALUE = FIFTH_ORDER
    };

    // returns paths to each hrtf file
    static std::vector<std::string> getAvailableConfigs();

    explicit HrtfManager(DecodingMatrixOrder hrtfConfig);
    HrtfManager();
    bool loadConfig(DecodingMatrixOrder hrtfConfig);

    std::vector<float> encodingMatrix(float azimuth, float elevation);

    const std::vector<std::array<float, irLength>>& getLeftHrtfValues() const;
    const std::vector<std::array<float, irLength>>& getRightHrtfValues() const;
    const std::vector<std::vector<float>>& getDecodingMatrix() const;

    std::vector<std::unique_ptr<fftconvolver::TwoStageFFTConvolver>>&
    getLeftConvolvers() ;
    std::vector<std::unique_ptr<fftconvolver::TwoStageFFTConvolver>>&
    getRightConvolvers() ;

private:
    bool loadMatrix(DecodingMatrixOrder hrtfConfig);
    bool loadHrtfs();

    std::vector<std::array<float, irLength>> mLeftHrtfValues;
    std::vector<std::array<float, irLength>> mRightHrtfValues;
    std::vector<std::vector<float>> mDecodingMatrix;
    DecodingMatrixOrder mHrtfConfig;
    bool mConfigLoaded = false;

    // vector that will contain left ear convolvers
    std::vector<std::unique_ptr<fftconvolver::TwoStageFFTConvolver>> mLeftConvolutionVector;
    // vector that will contain right ear convolvers
    std::vector<std::unique_ptr<fftconvolver::TwoStageFFTConvolver>> mRightConvolutionVector;
};

#endif /* HrtfManager_h */
