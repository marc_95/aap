/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "HrtfManager.h"
#include "Listener.h"
#include "OscManager.h"
#include "UIComponents.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent : public AudioAppComponent
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
    void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
    void releaseResources() override;

    //==============================================================================
    void paint(Graphics& g) override;
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...

    HrtfManager mHrtfManager;
    aap::Listener mListener;

    // Default value, TODO choose instead of harcode
    static const int mBlockSize = 512;

    const int mOutputChannels = 2;

    std::vector<std::vector<float>> mEncodedMatrix;
    std::vector<std::array<float, mBlockSize>> mDecodedMatrix;

    // TODO getFROM ui
    int mInputChannels                          = 2;
    HrtfManager::DecodingMatrixOrder mAmbiOrder = HrtfManager::DecodingMatrixOrder::THIRD_ORDER;

    UIComponents mUIComponents;
    OscManager mOscManager;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainComponent)
};
