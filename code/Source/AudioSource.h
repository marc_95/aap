//
//  AudioSource.hpp
//  AAP - App
//
//  Created by uni on 05/10/2019.
//

#ifndef AudioSource_hpp
#define AudioSource_hpp

#include "../JuceLibraryCode/JuceHeader.h"

#include <stdio.h>
#include <tuple>

// TODO implement LPFilter for elevation and azimuth

namespace aap
{
    class AudioSource;
};

class aap::AudioSource : public juce::Slider::Listener, public juce::ToggleButton::Listener
{
public:
    explicit AudioSource(int inputChannel);
    explicit AudioSource(const AudioSource& originalAudioSource);
    ~AudioSource();
    AudioSource const & operator=(AudioSource const &) const
    {
        return *this;
    }
    
    void setAzimuth(float azimuth);
    void setElevation(float elevation);
    void setDistance(float distance);
    void setActive(bool isActive);
    void setInputChannel(int inputChannel);

    std::pair<float, float> getAzimuthElevation() const;
    float getAzimuth() const;
    float getElevation() const;
    int getInputChannel() const;
    bool isActive() const;

    void sliderValueChanged(Slider* slider) override;
    void buttonClicked(Button* button) override;

private:
    // Angle of head rotation
    std::atomic<float> mAzimuth   = {0};
    std::atomic<float> mElevation = {0};
    float mDistance               = 1;
    int mInputChannel;
    std::atomic<bool> mActive = {false};
};
#endif /* AudioSource_hpp */
