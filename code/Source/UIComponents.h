//
//  UIComponents.hpp
//  AAP - App
//
//  Created by uni on 12/10/2019.
//

#ifndef UIComponents_hpp
#define UIComponents_hpp

#include <stdio.h>

#include "../JuceLibraryCode/JuceHeader.h"

class UIComponents
{
public:
    explicit UIComponents(int numOfInputs);
    float getVolumeValue() const;

    void resize(int width, int height);

private:
    Slider mVolumeSlider;
    Label mVolumeLabel;

    std::vector<std::unique_ptr<Slider>> mSourcesAzimuthSlider;
    std::vector<std::unique_ptr<Label>> mSourcesAzimuthLabel;

    std::vector<std::unique_ptr<Slider>> mSourcesElevationSlider;
    std::vector<std::unique_ptr<Label>> mSourcesElevationLabel;

    std::vector<std::unique_ptr<ToggleButton>> mSourcesActive;
    std::unique_ptr<Button> mCallibrateHeadTracking;

    friend class MainComponent;
};

#endif /* UIComponents_hpp */
